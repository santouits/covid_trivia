const api = require("./api.js");

let game_id = 0;
let question_id = 0;

class Question {
	constructor() {
		this.id = null;
		this.type = ""; // if it is boolean or multiple choice.
		this.category = ""; // general, science, history etc.
		this.answer = "";
		this.incorrect_answers = null;
		this.difficulty = "";
		this.text = "";
		this.answers = null;
		this.pos = -1;
	}
};

class Player {
	constructor(game, client) {
		this.client = client;
		/* We save the username so to attach client to player again if they disconnect */
		this.username = client.username;
		this.points = 0;
		this.color = "";

		game.players.push(this);
	}
};

class Game {
	constructor() {
		this.id = game_id++;
		this.started = false;
		this.players = [];
		this.questions = [];
		this.previous_questions = [];
		this.current_question = null;
		this.waiting_for_question = false; /* If players are waiting for a question */
		this.categories = [];
		for (let i = 0; i < api.categories_str.length; ++i) {
			this.categories.push(0);
		}
		//this.asked_for_next_question();
	}

	start() {
		if (this.started) return;
		this.started = true;
		this.waiting_for_question = true;
		this.send_next_question();
	}

	next_question() {
		this.waiting_for_question = true;
		this.send_next_question();
	}

	answer(info) {
		// find question by id
		let question = null;
		console.log(this.previous_questions);
		for (let q of this.previous_questions) {
			if (q.id == info.id) {
				question = q;
				break;
			}
		}

		let response = { id: info.id, error: false };
		
		if (!question) {
			response.error = true;
			return response;
		}

		response.correct = question.pos;

		if (question.pos == info.answer) {
			response.result = true;
		} else {
			response.result = false;
		}

		return response;
	}

	next() {


	}

	/* Send current question to a player that asked it */
	asked_for_current_question(player) {

	}

	update_settings(settings) {


	}

	fetch_questions() {
		if (!this.categories || this.categories.length == 0) {
			const query = new api.Query(null, this.difficulty, this.type, 30);
			api.fetch_questions(query, this);
		}

		else {
			for (const c of this.categories) {
				const query = new api.Query(c, this.difficulty, this.type, 10);
				api.fetch_questions(query, this);
			}
		}
	}

	/* Send next question to all connected players */
	/* Here we fetch questions from the database if there aren't enough */
	send_next_question() {
		if (this.questions.length == 0) {
			this.fetch_questions();
			return;
		}

		this.do_send_next_question();
	}

	fetched_questions(questions) {
		console.log("fetched questions");
		this.questions = this.questions.concat(questions);
		
		if (this.waiting_for_question) {
			this.waiting_for_question = false;
			this.do_send_next_question();

		}
	}

	create_question() {
		console.log("Creating question");
		const pos = Math.floor(Math.random() * Math.floor(this.questions.length));
		const info = this.questions.splice(pos, 1)[0];

		const question = new Question();
		question.id = question_id++;
		this.previous_questions.push(question);
		question.info = info;
		
		let answers_num;
		if (info.type == "multiple") {
			answers_num = 4;
		} else {
			answers_num = 2;
		}
		question.pos = Math.floor(Math.random() * Math.floor(answers_num));

		question.answers = [...info.incorrect_answers];
		question.answers.splice(question.pos, 0, info.correct_answer);

		return question;
	}

	/* We have already checked and there are enough questions to send */
	do_send_next_question() {
		if (this.questions.length == 0) {
			console.log("This shouldn't have happened, there are no questions fetched");
			return;
		}

		const question = this.create_question();
		this.current_question = question;

		for (let p of this.players) {
			p.client.socket.emit("new question", {
				"question" : question.info.question,
				"answers" : question.answers,
				"id": question.id
			});
		}
	}

	emit_question(player, question) {
		player.socket.emit(question);
	}

	player_disconnected(player) {
		var index = this.players.indexOf(player);
		if (index !== -1) {
			this.players.splice(index, 1);
		}

		/* Send to other clients */
}
};

function create_game(client) {
	console.log("creating game");
	const game = new Game();
	if (!game) {
		console.log("Error, couldn't create game");
		return null;
	}

	const player = new Player(game, client);
	
	return game;
};

module.exports = {
	create: create_game,
};
