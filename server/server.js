class Client {
	constructor(socket) {
		this.socket = socket;
		this.username = null;
		this.game = null;
	}
};


module.exports = {
	Client,
};
