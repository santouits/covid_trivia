const express = require('express');
const path = require('path');
const http = require('http');
const socket_io = require('socket.io');

const api = require("./server/api.js");
const game = require("./server/game.js");
const server = require("./server/server.js");

let clients = [];
let games = [];

const app = express();
const http_server = http.Server(app);
const io = socket_io(http_server);

app.use(express.static(path.join(__dirname, 'client'))); 

app.get('/', (req, res) => {
  res.sendFile(__dirname + './client/index.html');
});

api.fetch_categories();

io.on('connection', (socket) => {
	console.log('a user connected');

	// Create client
	const client = new server.Client(socket);
	clients.push(client);

	// Delete client
	socket.on('disconnect', () => {
    console.log('user disconnected');
		for (let c = 0; c < clients.length; ++c) {
			if (clients[c].socket == socket) {
				const disconnected = clients.splice(c, 1);
				const game = disconnected.game;
				if (!game) return;

				game.player_disconnected(client.player);
				disconnected.game = null;
				disconnected.player = null;
				disconnected.socket = null;
			}
		}
  });

	// Set username
	socket.on("set username", (username) => {
		client.username = username;	
		console.log("socket: " + socket + " set username to " + username);
	});

	socket.on('get options', () => {
		console.log("requested get options");
		socket.emit("options", { categories: client.game.categories });
	});

	socket.on("get categories", () => {
		console.log("asked for the categories");
		socket.emit("all categories", api.categories_str);
	});

	socket.on("create game", () => {
		console.log("asked to create game: ");// + settings);
		const new_game = game.create(client);
		games.push(new_game);
		client.game = new_game;

		socket.emit("game created", { id: new_game.id, categories: api.categories_str});
	});

	socket.on("start game", () => {
		client.game.start();
	});

	socket.on("next question", () => {
		client.game.next_question();
	});

	socket.on("current question", () => {
		console.log("asked for current question");
		const question = client.game.get_current_question();
	});

	socket.on("next question", () => {
		console.log();

	});

	socket.on("answered question", (answer) => {
		console.log("answered question")
		console.log(answer);
		const response = client.game.answer(answer);
		console.log(response);
		socket.emit("you were...", response);

	});

	socket.on('chat message', msg => {
    io.emit('chat message', msg);
  });
});

http_server.listen(3000, () => {
  console.log('listening on *:3000');
});

