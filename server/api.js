const https = require('https');

let CATEGORIES;
let categories_str = [];

const DIFFICULTY = ["Easy", "Medium", "Hard"];

const TYPE = ["Multiple Choice", "True / False"];

class Query {
	constructor(category, difficulty, type, count) {
		this.category = category;
		this.difficulty = difficulty;
		this.type = type;
		this.count = count;
	}
};

function fetch_categories() {
	console.log("Fetching Categories");
	https.get('https://opentdb.com/api_category.php', (res) => {
		let json = '';
		res.on('data', function (chunk) {
				json += chunk;
		});
		res.on('end', function () {
			if (res.statusCode === 200) {
				try {
					var data = JSON.parse(json);
					CATEGORIES = data.trivia_categories;
					for (c of CATEGORIES) {
						categories_str.push(c.name);
					}
				} catch (e) {
					console.log('Error parsing JSON!');
				}
			} else {
				console.log('Status:', res.statusCode);
			}
		});
	}).on('error', function (err) {
			console.log('Error:', err);
	});
};

function get_category_id(category) {
	for (c of CATEGORIES) {
		if (c.name == category) {
			return c.id;
		}
	}
	return 0;
};

function create_query_url(query) {
	console.log("create query url");
	console.log(query);
	if (!CATEGORIES) {
		console.log("Categories aren't fetched yet");
		return null;
	}
	const url = "https://opentdb.com/api.php?";
	let count_url = "";
	let category_url = "";
	let type_url = "";
	let difficulty_url = "";
	
	if (!query.count) query.count = 10;
	count_url = "amount=" + query.count;

	if (!query.category) {
		category_url = "";
	} else {
		category_url = "&category=" + get_category_id(query.category);
	}

	if (!query.difficulty) {
		difficulty_url = "";
	} else {
		difficulty_url = "&difficulty=" + query.difficulty;
	}

	if (!query.type) {
		type_url = "";
	} else {
		type_url = "&type=" + query.type;
	}

	let end_url = url + count_url + category_url + type_url + difficulty_url;
	console.log(end_url);
	return end_url;

};

function fetch_questions(query, game) {
	let query_url = create_query_url(query);
	https.get(query_url, (res) => {
		let json = '';
		res.on('data', function (chunk) {
			json += chunk;
		});
		res.on('end', function () {
			if (res.statusCode === 200) {
					try {
						var data = JSON.parse(json);
						game.fetched_questions(data.results);
					} catch (e) {
						console.log('Error parsing JSON!');
					}
			} else {
				console.log('Status:', res.statusCode);
			}
		});
	}).on('error', function (err) {
			console.log('Error:', err);
	});
};

module.exports = {
	fetch_categories,
	fetch_questions,
	categories_str,
	Query
};
