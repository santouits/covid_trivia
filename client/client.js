import { load_chat } from "./chat.js";

let socket;

const colors = ["white", "black", "yellow", "orange", "red", "pink", "purple", "blue", "aqua", "brown"];
let categories;

const state = {
	socket : null,
	username: "marx",
	color: "white",
	div: null,
	games: [],
	current_game: null,
};


const question = {
	div: null,
	text: null,
	answer1: null,
	answer2: null,
	answer3: null,
	answer4: null,
	type: null,
	answered: false,
	my_answer: -1
};

class Player {
	constructor() {
		this.username = null;
		this.color = "";
		this.points = 0;
	}
};

const game = {
	id: -1,
	players: [],
	started: false,
	time: null,
	question: null,
	categories: null,
	checked_categories: null
};

const main_page = {
	div: null,
	username_button: null,
	games: null,
	create_button: null,
};

const game_page = {
	username_button: null,
	options_button: null,
	start_button: null,
	game_options: null,
	options_open: false,
	start_button: null,
	players: null,
	question: null,
	answers_div: null,
	next_question_button: null
};

const options_page = {
	div: null,
	back_button: null,
	open: false,
	form: null
};

function create_button() {
	const b = document.createElement('button');
	const black = "#555555";
	b.style.margin = "10px";
	b.style.backgroundColor = "white";
	b.style.border = "2px solid " + black;
	b.style.color = "black";
	b.style.padding = "12px 28px";
	b.style.fontSize = "16px";
	b.style.textDecoration = "none";
	b.style.borderRadius = "6px";

	return b;
}


function reset_game() {
	game.id = -1;
	game.started = false;
	game.question = null;
	game.players = [];
	game.time = null;
	game.categories = null;
}

function create_username_button() {
	const button = create_button();

	//button.style.margin = "10px";
	button.addEventListener('click', function(e) {
		let value = window.prompt("set username","");
		if (value == null) return;
		if (value != "") {
			state.username = value;
			button.innerText = value;
			state.socket.emit('set username', value);
		}
	});
	return button;
};

 /*
 * It should show
 *  a button to set username,
 *	the current games to join,
 *	a button to create a game,
 *	the chat.
 **/
function client_main() {
	state.socket = io();

	create_main_page();
	create_game_page()
	create_game_options();

	register_callbacks();

	load_main_page();
}

function load_options_page() {
	document.body.removeChild(game_page.div);
	options_page.open = true;

	document.body.appendChild(options_page.div);

	state.socket.emit('get options', "");
}

function create_game_options() {
	const div = document.createElement("div");
	options_page.div = div;

	const bb = create_button();
	bb.innerText = "back";
	bb.addEventListener("click", () => {
		options_page.open = false;
		if (options_page.form) {
			options_page.div.removeChild(options_page.form);
		}
		options_page.form = null;
		load_game_page();
	});
	options_page.back_button = bb;
	div.appendChild(bb);

}

function create_game_page() {
	game_page.div = null;
	game_page.game_options = null;
	game_page.options_button = null;
	game_page.options_open = false;
	game_page.username_button = null;

	document.body.innerHTML = "";
	const div = document.createElement('div');
	div.style.textAlign = "center";
	game_page.div = div;

	game_page.username_button = create_username_button();
	div.appendChild(game_page.username_button);

	//div.appendChild(document.createElement("br"));
	const sb = create_button();
	sb.innerText = "options";
	sb.addEventListener("click", () => {
		load_options_page();
	});
	game_page.options_button = sb;
	div.appendChild(sb);
	//div.appendChild(document.createElement("br"));

	const start_button = create_button();
	start_button.innerText = "start";
	start_button.addEventListener("click", () => {
		state.socket.emit("start game");
	});
	game_page.start_button = start_button;
	div.appendChild(start_button);

	const q = document.createElement("div");
	game_page.question = q;
	div.appendChild(q);

	const answers = document.createElement("div");
	game_page.answers_div = answers;
	div.appendChild(answers);

	//div.appendChild(document.createElement("br"));
}


function close_game() {

}

function load_main_page() {
	document.body.innerHTML = "";
	document.body.appendChild(main_page.div);
	if (state.username == "") {
		main_page.username_button.innerText = "set username"
	} else {
		main_page.username_button.innerText = state.username;
	}
}

function load_game_page() {
	document.body.innerHTML = "";
	document.body.appendChild(game_page.div);
	if (state.username == "") {
		game_page.username_button.innerText = "set username"
	} else {
		game_page.username_button.innerText = state.username;
	}

}

function create_main_page() {
	main_page.div = null;
	main_page.username_button = null;
	main_page.games = null;
	main_page.create_button = null;

	main_page.div = document.createElement('div');
	main_page.div.style.textAlign = "center";
	
	const ub = create_username_button();
	main_page.username_button = ub;
	main_page.div.appendChild(ub);
	main_page.div.appendChild(document.createElement("br"));

	const create_game_button = create_button();
	create_game_button.addEventListener("click", () => {
		if (state.username == "") {
			window.alert("Set a username first.");
			return;
		}
		load_game_page();
		state.socket.emit("create game");
	});
	create_game_button.innerText = "create game";
	main_page.div.appendChild(create_game_button);

	//load_create_game();
	//load_current_games();
	

	//load_chat(state);

}

function register_callbacks() {
	state.socket.on("game created", (msg) => {
		console.log("game created with id: " + msg.id);
		reset_game();
		game.id = msg.id;
		game.categories = msg.categories;

		const player = new Player();
	});

	state.socket.on("options", (msg) => {

		console.log(msg.categories);
		game.checked_categories = msg.categories;
		create_game_form();
	});

	state.socket.on("new question", (msg) => {
		console.log("new question");
		game.question = msg;
		console.log(msg.question);
		console.log(msg.answers);
		update_question();
	});

	state.socket.on("you were...", (msg) => {
		if (msg.error) {
			console.log("server didn't find the question");
			return;
		}
		if (game.question.id == msg.id) {
			if (msg.result) {
				game_page.answers_div.children[game.question.my_answer].style.backgroundColor = "green";
				game_page.answers_div.children[game.question.my_answer].style.color = "white";
			} else {
				game_page.answers_div.children[game.question.my_answer].style.backgroundColor = "red";
				game_page.answers_div.children[game.question.my_answer].style.color = "white";
				game_page.answers_div.children[msg.correct].style.backgroundColor = "green";
				game_page.answers_div.children[msg.correct].style.color = "white";
			}
		}

		const next_div = document.createElement("div");
		const next_button = create_button();
		next_button.innerText = "next";
		next_button.addEventListener("click", () => {
			state.socket.emit("next question");
		});
		next_div.appendChild(next_button);
		game_page.answers_div.appendChild(next_div);

	});
}

function update_question() {
	game_page.question.innerText = game.question.question;
	game_page.answers_div.innerHTML = "";
	const question = game.question;
	for (let i = 0; i < question.answers.length; ++i) {
		const b = create_button();
		b.innerText = question.answers[i];
		b.addEventListener("click", () => {
			if (question.answered) {
				console.log("Already answered");
				return;
			}
			question.answered = true;
			question.my_answer = i;
			state.socket.emit("answered question", { id: question.id, answer: i })
		});
		game_page.answers_div.appendChild(b);
	}
}

function create_game_form() {
	console.log("create game form");
	if (!options_page.open) return;

	const form = document.createElement("div");
	options_page.form = form;
	const categories_div = document.createElement("div");
	const categories_checkboxes = []
	for (let c = 0; c < game.categories.length; ++c) {
		const input = document.createElement("input");
		if (game.checked_categories == 1) input.checked = true;
		categories_checkboxes.push(input);
		input.type = "checkbox";
		input.id = c;
		input.value = game.categories[c];
		input.name = c
		categories_div.appendChild(input);
		const label = document.createElement("label");
		label.for = c;
		label.innerText = game.categories[c];
		categories_div.appendChild(label);
		const br = document.createElement("br");
		categories_div.appendChild(br);
	}
	form.appendChild(categories_div);

	const button = create_button();
	button.innerText = "update options";
	button.addEventListener("click", function(e) {
    e.preventDefault();

		let selected_categories = [];
		for (let c of categories_checkboxes) {
			if (c.checked) {
				selected_categories.push(c.value);
				console.log(c.value);
			}
		}

		state.socket.emit("update_options", { id: game.id, categories: selected_categories });

  });
	form.appendChild(button);

	options_page.div.appendChild(form);


};

function join_button() {
	const button = create_button();
	button.innerText = "Fetch question";
	button.addEventListener('click', function(e) {
		state.socket.emit('question', "");
	});
	main_page.div.appendChild(button);
};

function create_question_div() {
  question.div = document.createElement('div');
	let text = document.createElement('div');
	let answer1 = create_button();
	let answer2 = create_button();
	let answer3 = create_button();
	let answer4 = create_button();
	question.text = text;
	question.answer1 = answer1;
	question.answer2 = answer2;
	question.answer3 = answer3;
	question.answer4 = answer4;
	question.div.appendChild(text);
	question.div.appendChild(answer1);
	question.div.appendChild(answer2);
	question.div.appendChild(answer3);
	question.div.appendChild(answer4);
};

client_main();
