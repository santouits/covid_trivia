let state;

export function load_chat(_state) {
	state = _state;
	let messages = document.createElement('ul');
	const form = document.createElement('form');
	const input = document.createElement('input');
	input.autocomplete = "off";
	const button = document.createElement('button');
	button.innerText = "Send"
	form.addEventListener('submit', function(e) {
		e.preventDefault();
		if (input.value) {
			state.socket.emit('chat message', state.username + ": " + input.value);
			input.value = '';
		}
	});

	state.socket.on('chat message', function(msg) {
		var item = document.createElement('li');
		item.textContent = msg;
		messages.appendChild(item);
		window.scrollTo(0, document.body.scrollHeight);
	});

	form.appendChild(input);
	form.appendChild(button);
	state.div.appendChild(messages);
	state.div.appendChild(form);
};
